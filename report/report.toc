\contentsline {section}{\numberline {1}Project Homepage}{2}{section.1}
\contentsline {section}{\numberline {2}Abstract}{2}{section.2}
\contentsline {section}{\numberline {3}Problem Statement and Goals}{3}{section.3}
\contentsline {section}{\numberline {4}Literature Review}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Bag of Features (BOF)}{4}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Histogram of Gradients(HOG)}{5}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Convolutional Neural Network(CNN)}{5}{subsection.4.3}
\contentsline {section}{\numberline {5}Prior and Related Work}{6}{section.5}
\contentsline {section}{\numberline {6}Project Formulation and Setup}{6}{section.6}
\contentsline {subsection}{\numberline {6.1}Support Vector Machines (SVM)}{6}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Random Forests and AdaBoost}{7}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Convolutional Neural Networks}{8}{subsection.6.3}
\contentsline {section}{\numberline {7}Methodology}{9}{section.7}
\contentsline {section}{\numberline {8}Implementation}{10}{section.8}
\contentsline {subsection}{\numberline {8.1}Feature Space}{10}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Preprocessing and Feature Extraction}{10}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Training, Validation and Model Selection}{11}{subsection.8.3}
\contentsline {section}{\numberline {9}Final Results}{12}{section.9}
\contentsline {subsection}{\numberline {9.1}Results with BOF features}{12}{subsection.9.1}
\contentsline {subsubsection}{\numberline {9.1.1}BOF with Random Forest}{12}{subsubsection.9.1.1}
\contentsline {paragraph}{Grid Search Results}{13}{section*.14}
\contentsline {subsubsection}{\numberline {9.1.2}BOF with SVM}{14}{subsubsection.9.1.2}
\contentsline {paragraph}{Grid Search Results}{14}{section*.15}
\contentsline {subsection}{\numberline {9.2}Results with HOG features}{15}{subsection.9.2}
\contentsline {subsubsection}{\numberline {9.2.1}HOG with Random Forest}{15}{subsubsection.9.2.1}
\contentsline {paragraph}{Grid Search Results}{16}{section*.16}
\contentsline {paragraph}{Classification Statistics}{16}{section*.17}
\contentsline {subsubsection}{\numberline {9.2.2}HOG with SVM}{17}{subsubsection.9.2.2}
\contentsline {paragraph}{Grid Search Results}{19}{section*.18}
\contentsline {paragraph}{Classification Statistics}{19}{section*.19}
\contentsline {subsection}{\numberline {9.3}Results with CNN}{20}{subsection.9.3}
\contentsline {paragraph}{Classification Statistics}{21}{section*.20}
\contentsline {paragraph}{Kaggle Competition Result}{22}{section*.21}
\contentsline {section}{\numberline {10}Interpretation}{22}{section.10}
\contentsline {section}{\numberline {11}Summary and Conclusions}{23}{section.11}
\contentsline {section}{\numberline {A}Software Tools}{25}{appendix.A}
